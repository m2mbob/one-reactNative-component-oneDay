#DatePickerIOS

使用`DatePickerIOS`在IOS上渲染一个日期/时间选择器。这是一个可控的组件，所以你必须绑定一个`onDateChange`回调并且更新`date`属性去更新组件，否则用户选择将作为可信的来源立刻设置到`props.date`。



###Props

> `date` Date

>

> 当前选择的日期

>

> `maximumDate` Date

>

> 最大的可选的日期/时间

>

> `minimumDate` Date

> 

> 最小的可选的日期/时间

>

> `minuteInterval` enum(1,2,3,4,5,6,10,15,20,30)

>

> 所选分钟的间隔

>

> mode enum('date','time','datetime')

>

> 日期选择器的模式

>

> onDateChange function

>

> 日期转换处理器

>

> 当用户改变日期时间组件时被调用。第一个也是唯一一个参数是一个代表了新的日期和时间的日期对象

>

> timeZoneOffsetInMinutes number

>

> 时区偏移，默认为当前时区。



### Examples

```

'use strict';

var React = require('react-native');

var {
  DatePickerIOS,
  StyleSheet,
  Text,
  TextInput,
  View

} = React;

var DatePickerExample = React.createClass({
  getDefaultProps: function () {
    return {
      date: new Date(),
      timeZoneOffsetInHours: (-1) * (new Date()).getTimezoneOffset() / 60,
    };
  },

  getInitialState: function() {
    return {
      date: this.props.date,
      timeZoneOffsetInHours: this.props.timeZoneOffsetInHours,
    };
  },

  onDateChange: function(date) {
    this.setState({date: date});
  },

  onTimezoneChange: function(event) {
    var offset = parseInt(event.nativeEvent.text, 10);
    if (isNaN(offset)) {
      return;
    }
    this.setState({timeZoneOffsetInHours: offset});
  },

  render: function() {
    // Ideally, the timezone input would be a picker rather than a    // text input, but we don't have any pickers yet :(    

    return (
      <View>
        <WithLabel label="Value:">
          <Text>{
            this.state.date.toLocaleDateString() +
            ' ' +
            this.state.date.toLocaleTimeString()
          }</Text>
        </WithLabel>
        <WithLabel label="Timezone:">
          <TextInput
            onChange={this.onTimezoneChange}
            style={styles.textinput}
            value={this.state.timeZoneOffsetInHours.toString()}
          />
          <Text> hours from UTC</Text>
        </WithLabel>
        <Heading label="Date + time picker" />
        <DatePickerIOS
          date={this.state.date}
          mode="datetime"
          timeZoneOffsetInMinutes={this.state.timeZoneOffsetInHours * 60}
          onDateChange={this.onDateChange}
        />
        <Heading label="Date picker" />
        <DatePickerIOS
          date={this.state.date}
          mode="date"
          timeZoneOffsetInMinutes={this.state.timeZoneOffsetInHours * 60}
          onDateChange={this.onDateChange}
        />
        <Heading label="Time picker, 10-minute interval" />
        <DatePickerIOS
          date={this.state.date}
          mode="time"
          timeZoneOffsetInMinutes={this.state.timeZoneOffsetInHours * 60}
          onDateChange={this.onDateChange}
          minuteInterval={10}
        />
      </View>
    );
  }

});

var WithLabel = React.createClass({
  render: function() {
    return (
      <View style={styles.labelContainer}>
        <View style={styles.labelView}>
          <Text style={styles.label}>
            {this.props.label}
          </Text>
        </View>
        {this.props.children}
      </View>
    );
  }});

var Heading = React.createClass({
  render: function() {
    return (
      <View style={styles.headingContainer}>
        <Text style={styles.heading}>
          {this.props.label}
        </Text>
      </View>
    );
  }});

exports.displayName = (undefined: ?string);

exports.title = '<DatePickerIOS>';

exports.description = 'Select dates and times using the native UIDatePicker.';

exports.examples = [{
  title: '<DatePickerIOS>',
  render: function(): 

ReactElement {
    return <DatePickerExample />;
  }

}];

var styles = StyleSheet.create({
  textinput: {
    height: 26,
    width: 50,
    borderWidth: 0.5,
    borderColor: '#0f0f0f',
    padding: 4,
    fontSize: 13,
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 2,
  },
  labelView: {
    marginRight: 10,
    paddingVertical: 2,
  },
  label: {
    fontWeight: '500',
  },
  headingContainer: {
    padding: 4,
    backgroundColor: '#f6f7f8',
  },
  heading: {
    fontWeight: '500',
    fontSize: 14,
  }

});

```



