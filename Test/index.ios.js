'use strict';

import React from 'react-native';
import Index from './app/Index';

const {
  AppRegistry,
  StyleSheet,
  NavigatorIOS
} = React;

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: '#111111'
  }
});

class Test extends React.Component{
  render() {
    return (
      <NavigatorIOS
        style={styles.container}
        initialRoute={{
          title: '首页',
          component: Index
        }}
        barTintColor='#FF83FA' />
    );
  }
}

AppRegistry.registerComponent('Test', () => Test);

