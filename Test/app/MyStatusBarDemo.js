'use strict';

import React from 'react-native';

const {
	StatusBarIOS,
	View,
	Text,
	StyleSheet,
	TouchableHighlight,
	ScrollView
} = React;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: 64
	},
    item: {
  		flex: 1,
  		height: 100,
  		borderWidth: 1,
  		borderColor: 'red',
  		justifyContent: 'center',
  		alignItems: 'center'
  	}
});

export default class MyStatusBarDemo extends React.Component{
	render(){
		return (
			<ScrollView style={styles.container}>
				<View>
			        {['default', 'light-content'].map((style) =>
			          <TouchableHighlight style={styles.item}
			            onPress={() => StatusBarIOS.setStyle(style, true)}>
			              <Text>setStyle('{style}',true)</Text>
			          </TouchableHighlight>
			        )}
		      	</View>
		      	<View>
			        {['none', 'fade', 'slide'].map((animation) =>
			          <View>
			            <TouchableHighlight style={styles.item}
			              onPress={() => StatusBarIOS.setHidden(true, animation)}>
			                <Text>setHidden(true, '{animation}')</Text>
			            </TouchableHighlight>
			            <TouchableHighlight style={styles.item}
			              onPress={() => StatusBarIOS.setHidden(false, animation)}>
			                <Text>setHidden(false, '{animation}')</Text>
			            </TouchableHighlight>
			          </View>
			        )}
		      	</View>
		      	<View>
			        <TouchableHighlight style={styles.item}
			          onPress={() => StatusBarIOS.setNetworkActivityIndicatorVisible(true)}>
			            <Text>setNetworkActivityIndicatorVisible(true)</Text>
			        </TouchableHighlight>
			        <TouchableHighlight style={styles.item}
			          onPress={() => StatusBarIOS.setNetworkActivityIndicatorVisible(false)}>
			            <Text>setNetworkActivityIndicatorVisible(false)</Text>
			        </TouchableHighlight>
			    </View>
	      	</ScrollView>
      	)
	}
}
