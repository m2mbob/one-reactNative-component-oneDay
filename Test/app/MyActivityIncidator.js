'use strict';

import React from 'react-native';

const {
	StyleSheet,
	View,
	Text,
	ActivityIndicatorIOS,
	TouchableHighlight
} = React;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: 64
	},
    item: {
  		flex: 1,
  		height: 100,
  		borderWidth: 1,
  		borderColor: 'red',
 		alignItems: 'center',
  		flexDirection: 'row',
        justifyContent: 'space-around'
    }
});

export default class MyActivityIncidator extends React.Component{
	constructor(props){
		super(props);
		this.state = {animating: true, status: 'stop'};
	}
	onPress(){
		this.setState({animating: !this.state.animating, status: (this.state.status==='start' ? 'stop' : 'start')});
	}
	render(){
		return(
			<View style={styles.container}>
				<View style={styles.item}>
					<ActivityIndicatorIOS animating={this.state.animating} color="#0000ff" size={'large'}/>
			        <ActivityIndicatorIOS animating={this.state.animating} color="#aa00aa" size={'large'}/>
			        <ActivityIndicatorIOS animating={this.state.animating} color="#aa3300" size={'large'}/>
			        <ActivityIndicatorIOS animating={this.state.animating} color="#00aa00" size={'large'}/>
				</View>
				<View style={styles.item}>
					<ActivityIndicatorIOS animating={this.state.animating} color="#0000ff" size={'small'}/>
			        <ActivityIndicatorIOS animating={this.state.animating} color="#aa00aa" size={'small'}/>
			        <ActivityIndicatorIOS animating={this.state.animating} color="#aa3300" size={'small'}/>
			        <ActivityIndicatorIOS animating={this.state.animating} color="#00aa00" size={'small'}/>
				</View>
				<TouchableHighlight style={styles.item} onPress={this.onPress.bind(this)}>
		          <Text>{'click to '+this.state.status}</Text>
		        </TouchableHighlight>
			</View>
		);
	}
}