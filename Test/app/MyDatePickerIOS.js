'use strict';

import React from 'react-native';

let {
	StyleSheet,
	View,
	DatePickerIOS,
	AlertIOS
} = React;

let styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: 64
	},
    item: {
  		flex: 1,
  		height: 100,
  		borderWidth: 1,
  		borderColor: 'red'
    }
});

export default class MyDatePickerIOS extends React.Component {
	constructor(props){
		super(props);
		this.state = {time: new Date()};
	}
	render(){
		return (
			<View style={styles.container}>
				<View style={styles.item}>
					<DatePickerIOS
					  date={this.state.time}
					  minuteInterval={10}
					  mode={'datetime'}/>
				</View>
				<View style={styles.item}>
					<DatePickerIOS
					  date={this.state.time}
					  minuteInterval={10}
					  mode={'date'}/>
				</View>
				<View style={styles.item}>
					<DatePickerIOS
					  date={this.state.time}
					  minuteInterval={10}
					  mode={'time'}
					  onDateChange={(newTime) =>{
					  	let lastTime = this.state.time;
					  	this.setState({time: newTime});
					  	AlertIOS.alert("确定选择如下时间",newTime,[
						    {text: '确定', onPress: () => console.log('success')},
						    {text: '取消', onPress: () => this.setState({time: lastTime})},
						  ]);
					  }}/>
				</View>
			</View>
		)
	}
}