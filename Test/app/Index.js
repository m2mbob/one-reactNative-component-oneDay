'use strict';

import React from 'react-native';
import MyDatePickerIOS from './MyDatePickerIOS';
import MyActivityIncidator from './MyActivityIncidator';
import MyListView from './MyListView';
import MyStatusBarDemo from './MyStatusBarDemo';

const {
  View,
  StyleSheet,
  Text,
  TouchableHighlight
} = React;

const styles = StyleSheet.create({
	container:{
		flex: 1,
		marginTop: 64
	},
  item: {
    flex: 1,
		borderWidth: 1,
		borderColor: 'red',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default class Index extends React.Component{
  onPress(route) {
    this.props.navigator.push(route);
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight style={styles.item} onPress={this.onPress.bind(this,{title:"MyDatePickerIOS",component:MyDatePickerIOS})}>
          <Text style={{borderWidth:1,borderColor:'blue'}}>Welcome to the MyDatePickerIOS . Press here!</Text>
        </TouchableHighlight>
        <TouchableHighlight style={styles.item} onPress={this.onPress.bind(this,{title:"MyActivityIncidator",component:MyActivityIncidator})}>
          <Text style={{borderWidth:1,borderColor:'blue'}}>Welcome to the MyActivityIncidatorIOS . Press here!</Text>
        </TouchableHighlight>
        <TouchableHighlight style={styles.item} onPress={this.onPress.bind(this,{title:"MyListView",component:MyListView})}>
          <Text style={{borderWidth:1,borderColor:'blue'}}>Welcome to the MyListView . Press here!</Text>
        </TouchableHighlight>
        <TouchableHighlight style={styles.item} onPress={this.onPress.bind(this,{title:"MyStatusBarDemo",component:MyStatusBarDemo})}>
          <Text style={{borderWidth:1,borderColor:'blue'}}>Welcome to the MyStatusBarDemo . Press here!</Text>
        </TouchableHighlight>
      </View>
    );
  }
}