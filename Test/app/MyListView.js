'use strict';

import React from 'react-native';

const {
	ScrollView,
	ListView,
	StyleSheet,
	View,
	Text,
	TouchableHighlight,
	ActivityIndicatorIOS,
	Image
} = React;

const REQUEST_URL = 'http://192.168.1.103:8080/estore/api/item/recommend.json';

const styles = StyleSheet.create({
  	item: {
	    flexDirection: 'row',
	    borderBottomWidth: 1,
	    borderColor: 'rgba(100, 53, 201, 0.1)',
	    paddingBottom: 6,
	    paddingTop: 6,
	},
  	itemContent: {
	    flex: 1,
	    marginLeft: 13,
	    marginTop: 20,
  	},
  	itemHeader: {
	    fontSize: 18,
	    fontFamily: 'Helvetica Neue',
	    fontWeight: '300',
	    color: '#6435c9',
	    marginBottom: 6,
  	},
  	itemMeta: {
	    fontSize: 16,
	    color: 'rgba(0, 0, 0, 0.6)',
	    marginBottom:6,
  	},
  	redText: {
	    color: '#db2828',
	    fontSize: 15,
  	},
  	loading: {
	    flex: 1,
	    justifyContent: 'center',
	    alignItems: 'center',
  	},
  	backgroundImage: {
	    flex: 1,
	    resizeMode: 'cover',
  	},
  	image: {
	    width: 99,
	    height: 99,
	    margin: 6,
  	},
  	itemText: {
	    fontSize: 16,
	    fontFamily: 'Helvetica Neue',
	    fontWeight: '300',
	    color: 'rgba(0, 0, 0, 0.8)',
	    lineHeight: 26,
  	},
  	container: {
	    backgroundColor: '#eae7ff',
	    flex: 1,
	    marginTop: 64
  	}
});

export default class MyListView extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			items: new ListView.DataSource({
				rowHasChanged: (row1, row2) => row1 !== row2
			}),
			loaded: false
		};
		this.fetchData = this.fetchData.bind(this);
	}
	fetchData() {
	    fetch(REQUEST_URL)
	    	.then(response => response.json())
	        .then(responseData => {
	            this.setState({
	              items: this.state.items.cloneWithRows(responseData),
	              loaded: true
	            });
	        })
	        .done();
	}
	renderItemList(item) {
	    return (
	        <TouchableHighlight
	        	underlayColor="rgba(34, 26, 38, 0.1)">
	        	<View style={styles.item}>
	            	<View style={styles.itemImage}>
	            		<Image
	              			source={{uri: item.recommend_pic}}
	              			style={styles.image}/>
	          		</View>
	          		<View style={styles.itemContent}>
	            		<Text style={styles.itemHeader}>
	            			{item.item.name}
	            		</Text>
	            		<Text style={styles.itemMeta}>
	              			{item.reason}
	            		</Text>
	          		</View>
	        	</View>
	        </TouchableHighlight>
	    );
	}
	componentDidMount(){
		this.fetchData();
	}
	render(){
		if (!this.state.loaded) {
	        return (
	            <View style={styles.container}>
	                <View style={styles.loading}>
	            	<ActivityIndicatorIOS
	                	size="large"
	                	color="#6435c9"/>
	            </View>
	        </View>
	      );
	    }
	    return (
	        <View style={styles.container}>
		        <ListView
		            dataSource={this.state.items}
		            renderRow={this.renderItemList.bind(this)}/>
	        </View>
	    );
	}
}

