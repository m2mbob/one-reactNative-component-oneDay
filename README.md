#one-reactNative-component-oneDay

### 简介：每天翻译一个react-native组件，并尝试该组件各种用法。

### 日志
1. 11月2日，NavigatorIOS组件
2. 11月3日，ActivityIndicatorIOS组件、DatePickerIOS组件。今天尝试了一波以上三个组件，发现还是有很多坑的，文档也有错误，下次整理一下吧代码贴出来。
3. 11月4日，提交示例代码，记录一下坑。sublime生成的Date.now()会导致DataPickerIOS组件报错；存在缓存，相同的代码第二天就跑起来了；在使用es6时会有一些坑，另外使用class时mixin的问题还要研究下。先这样。
4. 11月4日，上传了ListView的代码，但是ListView本身东西比较多，翻译推迟。
5. 11月9日，因为体测和新苗论文耽误了几天，今天继续更新了StatusBarIOS的代码，并且修改DatePickerIOS的代码，AlertIOS中的两个静态函数也做了尝试。
